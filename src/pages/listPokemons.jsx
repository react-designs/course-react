/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useCallback, useRef } from "react"
import Call from '../config/axios'
import Skeleton from "react-loading-skeleton"
import Searcher from "../components/Searcher"

export default function ListPokemons(props) 
{
  const [pokemons, setPokemons] = useState([])
  const [pokemonsCopy, setPokemonsCopy] = useState([])
  const [loadPokemons, setLoadPokemons] = useState(true)
  const [time, setTime] = useState('')
  const [isTyping, setIsTyping] = useState(false)
  const [page, setPage] = useState({offset: 0, limit: 50})
  const [search, setSearch] = useState('')
  const loadMoreRef = useRef()
  let lastScroll = 0
  const $ = el => document.querySelector(el)

  useEffect(() => {
    getPokemons()
  }, [page]);
  useEffect(() => {
    const onscroll = () => {
      let s = $('.pokemon-list__searcher-pokemon')
      if (s)
      {
        let currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0 && lastScroll <= currentScroll) {
          lastScroll = currentScroll;
          s.style.padding = '15px 20px'
          $('#searcher').style.padding = '10px 45px 10px 15px'
        } else {
          lastScroll = currentScroll;
          if (currentScroll === 0) {
            $('#searcher').style.padding = '25px 45px 25px 15px'
            s.style.padding = '0px'
          }
        }
      }
    };
    window.addEventListener('scroll', onscroll, true)
    return () => window.removeEventListener('scroll', onscroll, true)
  }, []);
  useEffect(() => {
    let observer
    const onChangeScroll = async (entries) =>
    {
      const el = entries[0]
      if(el.isIntersecting)
      {
        setPage(prev => ({...prev, offset: prev.offset+prev.limit}))
      }
    }
    Promise.resolve(
      typeof IntersectionObserver !== 'undefined'
      ? IntersectionObserver :
      import('intersection-observer')
    ).then(() => 
    {
      observer = new IntersectionObserver(onChangeScroll, { rootMargin: '50px' })
      observer.observe(loadMoreRef.current)
    })

    return () => observer && observer.disconnect()
  }, [])

  const getPokemons = async () =>
  {
    if(page.offset < 1) setLoadPokemons(prev => true)
    if(!$('#searcher').value)
    {
      const res = await Call('GET', `/pokemon/?offset=${page?.offset}&limit=${page?.limit}`, false, null)
      if(!res.data.next)
      {
        return loadMoreRef.current.hidden = true
      }
      let newData = res.data.results.map(item => {
        item.img = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${item.url.split('/')[6]}.png`
        return item
      })
      setPokemons([...pokemons, ...newData])
      setPokemonsCopy([...pokemons, ...newData])
    }
    if(page.offset < 1) setLoadPokemons(prev => false)
  }
  const getInfoFromPokemon = async (url) =>
  {
    let id = url.split('/')[6]
    props.history.push(`/pokemon/${id}`)
  }
  const handleSearch = (e) =>
  {
    setIsTyping(prev => true)
    clearTimeout(time)
    const { value } = e.target
    const timer = setTimeout(() => {
      let list = [...pokemonsCopy]
      let search = list.filter(item => item.name.toString().toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").includes(value.toString().toLowerCase()))
      setIsTyping(prev => false)
      setPokemons(search)
      setSearch(value)
    }, 500);
    setTime(timer)
  }

  const ItemPokemon = useCallback(({name, img, handleClick}) =>
  {
    return (
      <div className="pokemon-list__item" onClick={() => handleClick()}>
        <span className="pokemon-list__item-name">
          {name}
        </span>
        <div className="pokemon-list__img-wrap">
          <img src={`${img}`} alt={name} className="pokemon-list__img-wrap__img" />
        </div>
      </div>
    )
  }, [])

  return (
    <div className="pokemon-list">
      <div className="pokemon-list__searcher-pokemon">
        <Searcher searchFn={handleSearch} isTyping={isTyping}/>
      </div>
      {loadPokemons ? <Skeleton count={20} width={'100%'} className={'mb-1'} height={70}/> :
        pokemons && !loadPokemons ?
        pokemons?.map((item, index) => (
          item?.url &&
          <ItemPokemon key={index} handleClick={() => getInfoFromPokemon(item.url)} name={item.name} img={item.img}/>
        )) : 'no data :('
      }
      <div className="load-more mt-1" ref={loadMoreRef}>
        {!search &&
          <Skeleton count={1} width={'100%'} className={'mb-1'} height={70} />
        }
      </div>
    </div>
  );
}
