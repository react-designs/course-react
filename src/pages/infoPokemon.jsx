/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom'
import { FaRegHandPointLeft, FaLeaf } from 'react-icons/fa'
import { GiPunch } from 'react-icons/gi'
import { BsFillShieldFill } from 'react-icons/bs'
import { IoSpeedometerSharp } from 'react-icons/io5'
import Call from '../config/axios';
// import Skeleton from "react-loading-skeleton";
import { prominent } from 'color.js'

const InfoPokemon = (props) => 
{
  const [pokemon, setPokemon] = useState(null)
  const { id } = useParams()
  // const [showMore, setShowMore] = useState(false)

  useEffect(() => {
    getInfo()
  }, [id]);

  const $ = el => document.querySelector(el)

  const getInfo = async () =>
  {
    const res = await Call('GET', `/pokemon/${id}`, false, null,)
    setPokemon(res.data)
    const prominentColor = await prominent(res.data.sprites.front_default, {amount: 5, format: 'hex'})
    if(document.querySelector('.ellipse-wrap'))
    {
      $('.ellipse.--default').style.background = `${prominentColor[1]}`
      $('.ellipse.--small').style.background = `${prominentColor[2]}`
      $('.ellipse.--large').style.background = `${prominentColor[4]}`
    }
  }

  const Ellipses = useCallback(() =>
  {
    return (
      <div className="ellipse-wrap"> 
        <div className={`ellipse --default animated pulse`}></div>
        <div className={`ellipse --small animated pulse`}></div>
        <div className={`ellipse --large animated pulse`}></div>
      </div>
    )
  }, [])

  return (
    <div>
      <button className="back-button shadow-c" onClick={() => props.history.push('/')}>
        <FaRegHandPointLeft/>
      </button>
      {pokemon ? 
        <>
          <Ellipses/>
          <div className="wrap-info-pokemon mt-4">
            <img src={`${pokemon?.sprites.front_default}`} alt={`${pokemon?.name}`} className="wrap-info-pokemon__img" />
            <h3 className="fw-700 text-capitalize">{pokemon.name}</h3>
            <div className="wrap-info-pokemon__abilities mb-2 mt-3">
              {pokemon.abilities &&
                pokemon.abilities.map((item, index) => (
                  <div key={index} className="ability_item">
                    {item.ability.name || 'cargando'}
                  </div>
                ))
              }
            </div>
            <div className="wrap-info-pokemon__stats mt-3">
              {pokemon?.stats.map((item,index) => 
                !item?.stat.name.includes('special') &&
                <div key={index} className={`stat_item stat__${item.stat.name}`}>
                  <span className="value">
                    {item.base_stat || 0}
                  </span>
                  <span className="text">
                    {{
                      'hp': <FaLeaf/>,
                      'attack': <GiPunch />,
                      'defense': <BsFillShieldFill />,
                      'speed': <IoSpeedometerSharp />
                    }[item.stat.name]} {item.stat.name}
                  </span>
                </div>
              )}
            </div>
          </div>
        </>
        : 'Cargando...'
      }
    </div>
  );
}

export default InfoPokemon;
