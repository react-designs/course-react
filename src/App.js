import { Suspense, lazy } from 'react'
const MainRoute = lazy(() => import('./routes/main.route'))

function App() {
  return (
    <Suspense fallback={<p>cargando...</p>}>
      <MainRoute/>
    </Suspense>
  )
}

export default App