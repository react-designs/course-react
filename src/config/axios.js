import axios from 'axios'

const header = (method, url, isAuth, data, toSendAFile, progress, _export) => 
{
  let options = { method }        
  data && (options.data = data)  
  
  options.headers = {'Content-Type': toSendAFile ? 'multipart/form-data' : 'application/json'}
  axios.defaults.baseURL = 'https://pokeapi.co/api/v2/'
  
  if(progress.state)
  {
    options.onUploadProgress = progress.func
  }
  if (isAuth) 
  {    
    const token = localStorage.getItem('@token')
    token && (options.headers = {'Authorization': `${token}`, 'Content-Type': toSendAFile ? 'multipart/form-data' : 'application/json'})
    options.url = url
  } else {
    options.url = url
  }
  if(_export)
  {
    options.responseType = 'blob'
  }
  return options
}

const Call = (method, url, isAuth = false, data = null, toSendAFile = null, progress = {state: false, func: null}, _export = null) =>
new Promise((resolve, reject) => {
  axios(header(method, url, isAuth, data, toSendAFile, progress, _export))
  .then(res => resolve(res))
  .catch(err => reject(err))
})

export default Call