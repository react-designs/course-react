import React from 'react';
import { AiOutlineLoading } from 'react-icons/ai'
import { BsSearch } from 'react-icons/bs'

const Searcher = ({isTyping, searchFn}) => {
  return (
    <div>
      <div className="search-wrap">
        <input onChange={(e) => { searchFn(e) }} type="text" name="searcher" id="searcher" />
        {isTyping ? <AiOutlineLoading className="search-icon load" /> : <BsSearch className="search-icon" />}
      </div>
    </div>
  );
}

export default Searcher;
