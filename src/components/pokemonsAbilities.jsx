import React from 'react'

const Abilities = ({pokemon, setPokemon}) =>
{
  return (
    <div>
      <h5>{pokemon.name}</h5>
      <ul>
        {pokemon.abilities &&
          pokemon.abilities.map((item, index) => (
            <li key={index}>
              {item.ability.name}
            </li>
          ))
        }
      </ul>
    </div>
  )
}
export default Abilities